5 Benefits of Esports in Schools

As the esports industry has grown, academic esports teams have also become more common as an extracurricular activity. Students of all backgrounds are attracted to esports for a variety of reasons, but many are drawn in by the chance to compete while playing some of their favorite video games. Because esports is centered around the recreational video games that students play at home, many parents and teachers have struggled to understand how this extracurricular can support students’ educational goals.

Although esports is centered around an activity that students traditionally participate in recreationally, there are many benefits of esports in schools. Participation in school activities improves students’ wellbeing, and evidence shows that academic esports benefits students’ overall academic performance and social-emotional learning.

Here are five of the benefits of esports in schools:

Collaboration and teamwork skills
A successful esports team is centered around teamwork and communication. Similarly, to any sports team, students need to understand their team members’ skill sets and learn how to work together to complement each other’s abilities during competitions. A competitive esports team helps students take their gaming to the next level, learning leadership, sportsmanship, and communication skills as they play.

Increased academic and social engagement
An academic esports team gives students a way to channel their recreational video gaming into something that they can engage in at school alongside their peers. Students create connections with peers through their shared interest in gaming, and even the most disengaged students often begin looking forward to coming to school because of the community aspect of esports. This transcends to the classroom, where students are more motivated to maintain their academic performance and receive good grades in order to qualify for the esports team––much like any school sports team would require passing grades to be a member.

Exploration of STEM concepts and career paths
Esports offers a variety of ways for students to engage in STEM concepts, which can lead them to pursue STEM pathways in college and their careers. For example, students learn mathematics skills through their gameplay stats, utilizing data analysis and math to build a stronger strategy in the future. Additionally, esports plays an important part in students’ confidence with technology. Many students will begin exploring computers, processing, and even video game programming. Some students even learn to build their own computers to create the best gaming equipment.

Improved brain function and capabilities
There are several benefits of video gaming on the brain, with evidence that gaming can lead to enhanced visual perception, improved memory, and better attention and focus. These skills are not only helpful to students in their gaming, but also can have a positive effect on student wellbeing and academic performance. In the same way, to any extracurricular, being part of an esports team can improve students’ time management and task completion in school.

Tournaments offer peer connections and college and scholarship opportunities
Tournaments and peer connections are the main parts of academic esports that differentiate esports gaming from at-home gaming. Students compete alongside their team members in tournaments both virtually and in-person, with students from schools as close as the same district and as far as the other side of the world! Esports tournaments bring students from different backgrounds and cultures together through their shared interest in gaming. Academic esports even opens up college pathways for many students, with tournaments rewarding significant scholarships and universities recruiting students to participate on their esports teams.

Esports is organized competitive multiplayer video games.
Much like how when we talk about “sports,” it encompasses games/events like baseball, basketball, football and
swimming. There are a lot of games that fall into the realm
of esports, with some of the most popular being titles such
as League of Legends, Overwatch, Super Smash Bros.
Ultimate, Rocket League and, of course, Fortnite. But while
there may be those who dedicate many hours to playing
these games, it does not necessarily constitute competing
in esports. Though I may hit a baseball in a batting cage
for several hours, it does not mean I am a baseball player.
The difference is similar between “gaming” and “esports.”
Esports takes organization and dedication similar to
traditional sports.

Now esports is moving into education, with some
wondering if playing video games in their school is
appropriate. But would you be so quick to shut off your
child’s video game system if you knew that it could be a
portal to a scholarship at an international university, or a
career in a billion-dollar industry? What about if it led to
a reduction in your child’s anxiety, better connections
with his/her peers or an interest in developing better
sleep and nutritional habits?

In education, we are always looking to engage students in
ways we have not been able to in the past. Pew Research
shows 97% of boys and 83% of girls ages 13 to 17, identify
as being a gamer of some kind.1 Those demographics cut
across race and socio-economics. Imagine how different
our schools would look if those same demographics
identified as artists, or as dancers, or musicians. The
difference being no one is told they draw too much, or
dance too much, or play an instrument too much. The
messaging for video games has largely been they are a
waste of time. But this is the modern space children ar